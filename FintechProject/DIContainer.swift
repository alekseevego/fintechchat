//
//  DIContainer.swift
//  FintechProject
//
//  Created by Pavel Alekseev on 11/25/18.
//  Copyright © 2018 AlekseevEG. All rights reserved.
//

import Foundation
import Swinject

struct DIContainer {
    
    private var instance = Container { container in
        container.register(IConversationListContent.self) { r in
            ConversationListContent(storage: r.resolve(IStorage.self)!, communicationService: r.resolve(ICommunicationService.self)!)
        }
        
        container.register(IConversationContent.self) { r in
            ConversationContent(storage: r.resolve(IStorage.self)!, communicationService: r.resolve(ICommunicationService.self)!)
        }
        
        container.register(IProfileContent.self) { r in
            ProfileContent(storage: r.resolve(IStorage.self)!)
        }
        
        container.register(ICommunicationService.self) { r in
            CommunicationService(storage: r.resolve(IStorage.self)!)
            }.inObjectScope(.container)
        
        container.register(IStorage.self) { _ in Storage() }.inObjectScope(.container)
        
        container.register(IWebClient.self) { _ in WebClient() }
        
        container.register(IImageLoaderService.self) { r in
            ImageLoaderService(webClient: r.resolve(IWebClient.self)!)
        }
        
        container.register(IImageCollectionContent.self) { r in
            ImageCollectionContent(imageLoader: r.resolve(IImageLoaderService.self)!)
        }
    }
    
    func resolve<Service>() -> Service {
        return instance.resolve(Service.self)!
    }
    
    static let shared = DIContainer()
    
    private init() {
        
    }
}
