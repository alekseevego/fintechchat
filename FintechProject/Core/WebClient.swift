//
//  ImageLoaderService.swift
//  FintechProject
//
//  Created by Pavel Alekseev on 11/24/18.
//  Copyright © 2018 AlekseevEG. All rights reserved.
//

import Foundation
import UIKit

protocol IWebClient {
    func loadData(urlString : String, completion: @escaping (Data?, NSError?)->())
}

class WebClient : IWebClient {
    
    func loadData(urlString : String, completion: @escaping (Data?, NSError?)->()){
        guard let loadURL = URL(string : urlString) else {
            completion(nil, nil)
            print("Couldn't construct valid URL")
            return
        }
        let loadRequest = URLRequest(url:loadURL)
        URLSession.shared.dataTask(with: loadRequest, completionHandler: { (data, response, error) in
            if let error = error {
                completion(nil, error as NSError?)
                return
            }
            guard let data = data else {
                completion(nil, nil)
                return
            }
            completion(data, nil)
        }).resume()
    }
}
