//
//  Contact.swift
//  FintechProject
//
//  Created by Eugene Alekseev on 10/7/18.
//  Copyright © 2018 AlekseevEG. All rights reserved.
//

import Foundation

class Contact {
    let identifier: String
    var name: String?
    var messages: [Message] = []
    var date: Date?
    var online: Bool = false
    var hasUnreadMessages: Bool = false
    
    init(identifier: String, name: String?, messages: [Message] , date: Date, online: Bool, hasUnreadMessages: Bool){
        self.identifier = identifier
        self.name = name
        self.messages = messages
        self.online = online
        self.date = date
        self.hasUnreadMessages = hasUnreadMessages
    }
}
