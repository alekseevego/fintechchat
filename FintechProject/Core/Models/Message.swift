//
//  Message.swift
//  FintechProject
//
//  Created by Eugene Alekseev on 10/7/18.
//  Copyright © 2018 AlekseevEG. All rights reserved.
//

import Foundation
enum messageType : String, Codable {
    case textMessage = "TextMessage"
}
struct Message : Codable{
    let eventType:  messageType
    let identifier: String
    var text: String
    var incoming: Bool
}
