//
//  User.swift
//  FintechProject
//
//  Created by Eugene Alekseev on 10/26/18.
//  Copyright © 2018 AlekseevEG. All rights reserved.
//

import Foundation

struct Peer {
    let identifier: String
    let name: String
}
