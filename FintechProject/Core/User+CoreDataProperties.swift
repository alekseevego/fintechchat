//
//  User+CoreDataProperties.swift
//  FintechProject
//
//  Created by Pavel Alekseev on 11/11/18.
//  Copyright © 2018 AlekseevEG. All rights reserved.
//
//

import Foundation
import CoreData


extension User {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<User> {
        return NSFetchRequest<User>(entityName: "User")
    }

    @NSManaged public var name: String?
    @NSManaged public var identifier: String?
    @NSManaged public var online: Bool
    @NSManaged public var conversations: NSSet?

}

// MARK: Generated accessors for conversations
extension User {

    @objc(addConversationsObject:)
    @NSManaged public func addToConversations(_ value: Conversation)

    @objc(removeConversationsObject:)
    @NSManaged public func removeFromConversations(_ value: Conversation)

    @objc(addConversations:)
    @NSManaged public func addToConversations(_ values: NSSet)

    @objc(removeConversations:)
    @NSManaged public func removeFromConversations(_ values: NSSet)

}
