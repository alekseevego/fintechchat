//
//  Conversation+CoreDataProperties.swift
//  FintechProject
//
//  Created by Pavel Alekseev on 11/11/18.
//  Copyright © 2018 AlekseevEG. All rights reserved.
//
//

import Foundation
import CoreData


extension Conversation {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Conversation> {
        return NSFetchRequest<Conversation>(entityName: "Conversation")
    }

    @NSManaged public var name: String?
    @NSManaged public var conversationID: String?
    @NSManaged public var messages: NSOrderedSet?
    @NSManaged public var users: NSOrderedSet?

}

// MARK: Generated accessors for messages
extension Conversation {

    @objc(insertObject:inMessagesAtIndex:)
    @NSManaged public func insertIntoMessages(_ value: StoredMessage, at idx: Int)

    @objc(removeObjectFromMessagesAtIndex:)
    @NSManaged public func removeFromMessages(at idx: Int)

    @objc(insertMessages:atIndexes:)
    @NSManaged public func insertIntoMessages(_ values: [StoredMessage], at indexes: NSIndexSet)

    @objc(removeMessagesAtIndexes:)
    @NSManaged public func removeFromMessages(at indexes: NSIndexSet)

    @objc(replaceObjectInMessagesAtIndex:withObject:)
    @NSManaged public func replaceMessages(at idx: Int, with value: StoredMessage)

    @objc(replaceMessagesAtIndexes:withMessages:)
    @NSManaged public func replaceMessages(at indexes: NSIndexSet, with values: [StoredMessage])

    @objc(addMessagesObject:)
    @NSManaged public func addToMessages(_ value: StoredMessage)

    @objc(removeMessagesObject:)
    @NSManaged public func removeFromMessages(_ value: StoredMessage)

    @objc(addMessages:)
    @NSManaged public func addToMessages(_ values: NSOrderedSet)

    @objc(removeMessages:)
    @NSManaged public func removeFromMessages(_ values: NSOrderedSet)

}

// MARK: Generated accessors for users
extension Conversation {

    @objc(insertObject:inUsersAtIndex:)
    @NSManaged public func insertIntoUsers(_ value: User, at idx: Int)

    @objc(removeObjectFromUsersAtIndex:)
    @NSManaged public func removeFromUsers(at idx: Int)

    @objc(insertUsers:atIndexes:)
    @NSManaged public func insertIntoUsers(_ values: [User], at indexes: NSIndexSet)

    @objc(removeUsersAtIndexes:)
    @NSManaged public func removeFromUsers(at indexes: NSIndexSet)

    @objc(replaceObjectInUsersAtIndex:withObject:)
    @NSManaged public func replaceUsers(at idx: Int, with value: User)

    @objc(replaceUsersAtIndexes:withUsers:)
    @NSManaged public func replaceUsers(at indexes: NSIndexSet, with values: [User])

    @objc(addUsersObject:)
    @NSManaged public func addToUsers(_ value: User)

    @objc(removeUsersObject:)
    @NSManaged public func removeFromUsers(_ value: User)

    @objc(addUsers:)
    @NSManaged public func addToUsers(_ values: NSOrderedSet)

    @objc(removeUsers:)
    @NSManaged public func removeFromUsers(_ values: NSOrderedSet)

}
