//
//  StoredMessage+CoreDataProperties.swift
//  FintechProject
//
//  Created by Pavel Alekseev on 11/11/18.
//  Copyright © 2018 AlekseevEG. All rights reserved.
//
//

import Foundation
import CoreData


extension StoredMessage {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<StoredMessage> {
        return NSFetchRequest<StoredMessage>(entityName: "StoredMessage")
    }

    @NSManaged public var identifier: String?
    @NSManaged public var incoming: Bool
    @NSManaged public var text: String?
    @NSManaged public var date: NSDate?
    @NSManaged public var conversation: Conversation?

}
