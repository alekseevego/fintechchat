//
//  RequestFabric.swift
//  FintechProject
//
//  Created by Eugene Alekseev on 11/16/18.
//  Copyright © 2018 AlekseevEG. All rights reserved.
//

import Foundation
import CoreData

public class RequestFactory {
    
    static func conversation(id : String) -> NSFetchRequest<NSFetchRequestResult> {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: String(describing: Conversation.self))
        fetchRequest.predicate = NSPredicate(format: "conversationID = %@", id)
        return fetchRequest
    }
    
    static func message(id : String) -> NSFetchRequest<NSFetchRequestResult>{
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: String(describing: StoredMessage.self))
        fetchRequest.predicate = NSPredicate(format: "identifier = %@", id)
        return fetchRequest
    }
    
    static func conversationMessages(id: String) -> NSFetchRequest<NSFetchRequestResult> {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: String(describing: Conversation.self))
        fetchRequest.predicate = NSPredicate(format: "conversationID = %@", id)
        return fetchRequest
    }
    
    static func onlineUsers() -> NSFetchRequest<NSFetchRequestResult>{
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: String(describing: User.self))
        fetchRequest.predicate = NSPredicate(format: "online = %@", true)
        return fetchRequest
    }
    
    static func user(id: String) -> NSFetchRequest<NSFetchRequestResult>{
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: String(describing: User.self))
        fetchRequest.predicate = NSPredicate(format: "identifier = %@", id)
        return fetchRequest
    }
    
    static func activeConversations(userID : String) -> NSFetchRequest<NSFetchRequestResult> {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: String(describing: Conversation.self))
        fetchRequest.predicate = NSPredicate(format: "users[FIRST].online = %@", true)
        return fetchRequest
    }
    static func fetchRecordsForEntity(_ fetchRequest: NSFetchRequest<NSFetchRequestResult>, inManagedObjectContext managedObjectContext: NSManagedObjectContext) -> [NSManagedObject]{
        var result = [NSManagedObject]()
        do {
            if let records = try managedObjectContext.fetch(fetchRequest) as? [NSManagedObject] {
                result = records
            }
        } catch {
            print("Unable to fetch managed objects.")
        }
        return result
    }
}
