//
//  Storage.swift
//  FintechProject
//
//  Created by Eugene Alekseev on 11/1/18.
//  Copyright © 2018 AlekseevEG. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class Storage : IStorage{
    
    lazy var container: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "ChatModel")
        container.loadPersistentStores(completionHandler: {_, _ in})
        container.viewContext.automaticallyMergesChangesFromParent = true
        return container
    }()
    
    func loadProfile(callback: @escaping (UIImage?, String?, String?)->()){
        container.performBackgroundTask{ backgroundContext in
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: String(describing: Profile.self))
            if let result = RequestFactory.fetchRecordsForEntity(fetchRequest, inManagedObjectContext: backgroundContext).first as? Profile {
                var image: UIImage? = nil
                var description: String?
                var name: String?
                
                if let data = result.profileImage as Data? {
                    image = UIImage(data: data)!
                }
                if let descriptionText = result.profileDescription as String? {
                    description = descriptionText
                }
                if let nameText = result.profileName as String? {
                    name = nameText
                }
                callback(image, name, description)
            }
        }
    }
    
    func saveMessage(id: String, text: String, incoming : Bool, conversationID: String, date: Date) {
        container.performBackgroundTask{ backgroundContext in
            var message: StoredMessage
            let request = RequestFactory.message(id: id)
            if let storedMessage = RequestFactory.fetchRecordsForEntity(request, inManagedObjectContext: backgroundContext).first as? StoredMessage{
                message = storedMessage
            }
            else if let storedMessage = NSEntityDescription.insertNewObject(forEntityName: String(describing: StoredMessage.self), into: backgroundContext) as? StoredMessage {
                message = storedMessage
            } else {
                print("Error saving message")
                return
            }
            message.setValue(id, forKey: "identifier")
            message.setValue(text, forKey: "text")
            message.setValue(incoming, forKey: "incoming")
            message.setValue(date, forKey: "date")
            let crequest = RequestFactory.conversation(id: conversationID)
            if let conversation = RequestFactory.fetchRecordsForEntity(crequest, inManagedObjectContext: backgroundContext).first as? Conversation{
                message.conversation = conversation
            }
            do {
                backgroundContext.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy
                try backgroundContext.save()
            } catch {
                print("Cannot save message \(error)")
            }
        }
    }
    
    func saveUser(id: String, name: String, online: Bool){
        container.performBackgroundTask{ backgroundContext in
            var user: User
            let request = RequestFactory.user(id: id)
            if let storedUser = RequestFactory.fetchRecordsForEntity(request, inManagedObjectContext: backgroundContext).first as? User{
                user = storedUser
            }
            else if let storedUser = NSEntityDescription.insertNewObject(forEntityName: String(describing: User.self), into: backgroundContext) as? User {
                user = storedUser
            }else {
                print("Error saving user")
                return
            }
            user.setValue(id, forKey: "identifier")
            user.setValue(name, forKey: "name")
            user.setValue(online, forKey: "online")
            do {
                backgroundContext.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy
                try backgroundContext.save()
                let userDataDict:[String: User] = ["user": user]
                NotificationCenter.default.post(name: Notification.Name("userChanged"), object: nil, userInfo: userDataDict)
            } catch {
                print("Cannot save User")
            }
        }
    }
    
    func saveConversation(id: String, name: String?, messageID: String?, userID: String?) {
        container.performBackgroundTask{ backgroundContext in
            var conversation: Conversation
            let request = RequestFactory.conversation(id: id)
            if let storedConversation = RequestFactory.fetchRecordsForEntity(request, inManagedObjectContext: backgroundContext).first as? Conversation{
                conversation = storedConversation
            }
            else{
                if let storedConversation = NSEntityDescription.insertNewObject(forEntityName: String(describing: Conversation.self), into: backgroundContext) as? Conversation {
                    conversation = storedConversation
                } else {
                    print("Error saving user")
                    return
                }
            }
            conversation.setValue(id, forKey: "conversationID")
            if name != nil {
                conversation.setValue(name, forKey: "name")
            }
            if messageID != nil {
                let request = RequestFactory.message(id: id)
                if let message = RequestFactory.fetchRecordsForEntity(request, inManagedObjectContext: backgroundContext).first as? StoredMessage{
                    let m = conversation.messages?.mutableCopy() as! NSMutableOrderedSet
                    m.add(message)
                    conversation.setValue(m, forKey: "messages")
                }
            }
            if userID != nil {
                let request = RequestFactory.user(id: id)
                if let user = RequestFactory.fetchRecordsForEntity(request, inManagedObjectContext: backgroundContext).first as? User{
                    let u = conversation.users?.mutableCopy() as! NSMutableOrderedSet
                    u.add(user)
                    conversation.setValue(u, forKey: "users")
                }
            }
            do {
                backgroundContext.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy
                try backgroundContext.save()
            } catch {
                print("Cannot save Conversation \(error)")
            }
        }
    }
    func getUser(id: String, completion : @escaping (User?) -> ()){
        let request = RequestFactory.user(id: id)
        container.performBackgroundTask{ backgroundContext in
            if let storedUser = RequestFactory.fetchRecordsForEntity(request, inManagedObjectContext: backgroundContext).first as? User{
                completion(storedUser)
            }
            else{
                completion(nil)
            }
        }
    }
    func saveProfile(image: UIImage?,name: String?,description: String?, callback: @escaping ()->()) {
        container.performBackgroundTask{ backgroundContext in
            var profile: Profile
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: String(describing: Profile.self))
            if let p = RequestFactory.fetchRecordsForEntity(fetchRequest, inManagedObjectContext: backgroundContext).first as? Profile{
                profile = p
            }
            else if let p = NSEntityDescription.insertNewObject(forEntityName: String(describing: Profile.self), into: backgroundContext) as? Profile {
                profile = p
            } else {
                print("Error saving profile")
                return
            }
            if name != nil {
                profile.setValue(name, forKey: "profileName")
            }
            if description != nil {
                profile.setValue(description, forKey: "profileDescription")
            }
            if image != nil {
                if let imageData = UIImagePNGRepresentation(image!) {
                    let imageData = imageData as NSData
                    profile.setValue(imageData, forKey: "profileImage")
                }
            }
            do {
                backgroundContext.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy
                try backgroundContext.save()
            } catch {
                print("Cannot save Profile")
            }
            callback()
        }
    }
    
    func initializeFetchedResultsController(delegate : NSFetchedResultsControllerDelegate, entityName : String, sortDescriptors: [NSSortDescriptor], predicate: NSPredicate?) -> NSFetchedResultsController<NSFetchRequestResult> {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        if predicate != nil {
            request.predicate = predicate
        }
        request.sortDescriptors = sortDescriptors
        let fetchedResultsController = NSFetchedResultsController(fetchRequest: request, managedObjectContext: container.viewContext, sectionNameKeyPath: nil, cacheName: nil)
        fetchedResultsController.delegate = delegate
        do {
            try fetchedResultsController.performFetch()
        } catch {
            fatalError("Failed to initialize FetchedResultsController: \(error)")
        }
        return fetchedResultsController
    }
    
}
