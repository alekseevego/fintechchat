//
//  Profile+CoreDataProperties.swift
//  FintechProject
//
//  Created by Pavel Alekseev on 11/11/18.
//  Copyright © 2018 AlekseevEG. All rights reserved.
//
//

import Foundation
import CoreData


extension Profile {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Profile> {
        return NSFetchRequest<Profile>(entityName: "Profile")
    }

    @NSManaged public var profileDescription: String?
    @NSManaged public var profileImage: NSData?
    @NSManaged public var profileName: String?

}
