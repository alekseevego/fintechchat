//
//  AppDelegate.swift
//  FintechProject
//
//  Created by Eugene Alekseev on 9/21/18.
//  Copyright  2018 AlekseevEG. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var timer : Timer?
    var spawnPosition : CGPoint?
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        let window = UIWindow(frame: UIScreen.main.bounds)
        let longPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress))
        longPressGesture.delegate = self
        window.addGestureRecognizer(longPressGesture)
        window.makeKeyAndVisible()
        self.window = window
        let storyboard = UIStoryboard(name: "ConversationList", bundle: nil)
        guard let controller = storyboard.instantiateViewController(withIdentifier: "ConversationListViewController") as? ConversationListViewController
            else { fatalError("Cannot instantiate root view controller") }
        window.rootViewController = UINavigationController(rootViewController: controller)
        
        return true
    }
    @objc func handleLongPress(sender : UILongPressGestureRecognizer){
        spawnPosition = sender.location(ofTouch: 0, in: window)
        if sender.state == .began {
            startSpawnImages()
        }
        if sender.state == .ended {
            stopSpawnImages()
        }
    }
    func startSpawnImages() {
        if timer == nil {
            timer =  Timer.scheduledTimer(
                timeInterval: TimeInterval(0.2),
                target      : self,
                selector    : #selector(showEmblem),
                userInfo    : nil,
                repeats     : true)
        }
        
    }
    
    func stopSpawnImages() {
        if timer != nil {
            timer?.invalidate()
            timer = nil
        }
    }
    
    @objc func showEmblem(){
        let emblem = #imageLiteral(resourceName: "TinkoffEmblem")
        guard let spawnPosition = spawnPosition else {
            return
        }
        let imageView = UIImageView(frame: CGRect(x: spawnPosition.x - 25, y: spawnPosition.y - 25, width: 50,height: 50));
        imageView.image = emblem
        window?.addSubview(imageView)
        let initialPosition = imageView.frame.origin
        let randomX = CGFloat(Float(arc4random()) / Float(UINT32_MAX))*120 - 30
        let randomY = CGFloat(Float(arc4random()) / Float(UINT32_MAX))*120 - 30
        UIView.animate(withDuration: 0.4, animations: {
            imageView.center = CGPoint(x: initialPosition.x + randomX, y: initialPosition.y + randomY)
            imageView.alpha = 0.05
        }, completion: { _ in
            imageView.removeFromSuperview()
        })
    }
    func applicationWillResignActive(_ application: UIApplication) {
        //print("Application moved from active to inactive: \(#function)")
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        //print("Application moved from inactive to background: \(#function)")
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        //print("Application moved from background to inactive: \(#function)")
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        //print("Application moved from inactive to active: \(#function)")
    }

    func applicationWillTerminate(_ application: UIApplication) {
        let service : CommunicationService = DIContainer.shared.resolve()
        service.stop()
    }
}
extension AppDelegate : UIGestureRecognizerDelegate {
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        return true
    }
}

