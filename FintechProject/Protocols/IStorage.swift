//
//  IStorage.swift
//  FintechProject
//
//  Created by Eugene Alekseev on 11/4/18.
//  Copyright © 2018 AlekseevEG. All rights reserved.
//

import Foundation
import UIKit
import CoreData

protocol IStorage : class {
    
    func loadProfile(callback: @escaping (UIImage?, String?, String?)->())
    
    func saveProfile(image: UIImage?,name: String?,description: String?, callback: @escaping ()->())
    
    func saveMessage(id: String, text: String, incoming : Bool, conversationID: String, date: Date)
    
    func saveUser(id: String, name: String, online: Bool)
    
    func getUser(id: String, completion : @escaping (User?) -> ())
    
    func saveConversation(id: String, name: String?, messageID: String?, userID: String?)
    
    func initializeFetchedResultsController(delegate : NSFetchedResultsControllerDelegate, entityName : String, sortDescriptors : [NSSortDescriptor], predicate: NSPredicate?) -> NSFetchedResultsController<NSFetchRequestResult>
    
}
