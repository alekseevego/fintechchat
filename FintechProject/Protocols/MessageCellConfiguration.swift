protocol MessageCellConfiguration: class {
    var message: String? { get set }
}
