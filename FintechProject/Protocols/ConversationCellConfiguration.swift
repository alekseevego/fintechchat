//
//  ConversationCellConfiguration.swift
//  FintechProject
//
//  Created by Eugene Alekseev on 10/7/18.
//  Copyright © 2018 AlekseevEG. All rights reserved.
//

import Foundation

protocol ConversationCellConfiguration: class {
    var name: String? { get set }
    var message: StoredMessage? { get set }
    var date: Date? { get set }
    var online: Bool { get set }
    var hasUnreadMessages: Bool { get set }
}
