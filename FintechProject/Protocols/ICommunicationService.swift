//
//  ICommunicationService.swift
//  FintechProject
//
//  Created by Eugene Alekseev on 10/26/18.
//  Copyright © 2018 AlekseevEG. All rights reserved.
//

import Foundation

protocol ICommunicationService {
    
    /// Делегат сервиса
    var delegate: CommunicationServiceDelegate? { get set }
    
    /// Онлайн/Не онлайн
    var online: Bool { get set }
    
    /// Отправляет сообщение участнику
    func send(_ message: Message, to peer: Peer)
}
