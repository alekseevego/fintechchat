//
//  CommunicationServiceDelegate.swift
//  FintechProject
//
//  Created by Eugene Alekseev on 10/26/18.
//  Copyright © 2018 AlekseevEG. All rights reserved.
//

import Foundation
protocol CommunicationServiceDelegate: class {
    
    /// Browsing
    func communicationService(_ communicationService: ICommunicationService,
                              didFoundPeer peer: Peer)
    
    func communicationService(_ communicationService: ICommunicationService,
                              didLostPeer peer: Peer)
    
    func communicationService(_ communicationService: ICommunicationService,
                              didNotStartBrowsingForPeers error: Error)
    
    /// Advertising
    func communicationService(_ communicationService: ICommunicationService,
                              didReceiveInviteFromPeer peer: Peer,
                              invintationClosure: (Bool) -> Void)
    
    func communicationService(_ communicationService: ICommunicationService,
                              didNotStartAdvertisingForPeers error: Error)
    
    /// Messages
    func communicationService(_ communicationService: ICommunicationService,
                              didReceiveMessage message: Message,
                              from peer: Peer)
}
