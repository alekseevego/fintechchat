//
//  DataManagerProtocol.swift
//  FintechProject
//
//  Created by Pavel Alekseev on 10/21/18.
//  Copyright © 2018 AlekseevEG. All rights reserved.
//

import Foundation

protocol DataManagerProtocol : class {
    func loadData(key: String, callback:  @escaping ((String?)->()), errorHandle: @escaping (()->()))
    func saveData(key: String, value: String, errorHandle: @escaping (()->()))
}
