//
//  ImageCollectionContent.swift
//  FintechProject
//
//  Created by Pavel Alekseev on 11/25/18.
//  Copyright © 2018 AlekseevEG. All rights reserved.
//

import Foundation
import UIKit

protocol IImageCollectionContent {
    
    func getNumberOfObjects() -> Int
    
    func getObjectAt(indexPath: IndexPath) -> Hit
    
    func loadListOfImages(completion: @escaping ()->())
    
    func loadImage(loadURLString : String, _ completion: @escaping (_ image: UIImage?, _ loadedURL: String?, _ error: NSError?) -> ())
}

class ImageCollectionContent : IImageCollectionContent {
    
    private let imageLoader : IImageLoaderService
    private var imageUrls : [Hit] = []
    
    init(imageLoader : IImageLoaderService){
        self.imageLoader = imageLoader
    }
    func getNumberOfObjects() -> Int {
        return imageUrls.count
    }
    
    func getObjectAt(indexPath: IndexPath) -> Hit {
        return imageUrls[indexPath.row]
    }
    
    func loadListOfImages(completion: @escaping ()->())
    {
        imageLoader.loadImageList {[weak self] hits, error in
            if let error = error {
                print(error)
                return
            }
            self?.imageUrls = hits
            completion()
        }
    }
    func loadImage(loadURLString : String, _ completion: @escaping (_ image: UIImage?, _ loadedURL: String?, _ error: NSError?) -> ()) {
        imageLoader.loadImage(loadURLString: loadURLString, completion)
    }

}
