//
//  ImageCollectionViewCell.swift
//  FintechProject
//
//  Created by Pavel Alekseev on 11/24/18.
//  Copyright © 2018 AlekseevEG. All rights reserved.
//

import UIKit

class ImageCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    override func awakeFromNib() {
        super.awakeFromNib()
        activityIndicator.hidesWhenStopped = true;
        // Initialization code
    }
    
}
