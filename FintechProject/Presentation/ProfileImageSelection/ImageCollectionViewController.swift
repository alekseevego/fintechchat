//
//  ImageCollectionViewController.swift
//  FintechProject
//
//  Created by Pavel Alekseev on 11/24/18.
//  Copyright © 2018 AlekseevEG. All rights reserved.
//

import UIKit

final class ImageCollectionViewController: UIViewController {
    
    private let cellIdentifier = "ImageCell"
    private let sectionInsets = UIEdgeInsets(top: 2.5, left: 2.5, bottom: 2.5, right: 2.5)
    private let itemsPerRow: CGFloat = 3
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    var content : IImageCollectionContent =  DIContainer.shared.resolve()
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureCollectionView()
        activityIndicator.hidesWhenStopped = true
        activityIndicator.startAnimating()
        content.loadListOfImages{
            DispatchQueue.main.async { [weak self] in
                self?.activityIndicator.stopAnimating()
                self?.collectionView.reloadData()
            }
        }
    }

    func configureCollectionView() {
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(UINib(nibName: "ImageCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: cellIdentifier)
    }
    
    @IBAction func closeBtnClicked() {
        self.dismiss(animated: true, completion: nil)
    }
}
extension ImageCollectionViewController : UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return content.getNumberOfObjects()
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        self.activityIndicator.startAnimating()
                if let presenter = presentingViewController as? ProfileViewController {
            content.loadImage(loadURLString: content.getObjectAt(indexPath: indexPath).largeImageURL){ image, _, error in
                if let error = error {
                    print(error)
                    return
                }
                if let image = image {
                    presenter.setImage(image: image)
                }
                else {
                    print("Couldn't set new profile image cause loaded image was nil")
                    //TODO: alert here to let user know
                }
                DispatchQueue.main.async {
                    self.activityIndicator.stopAnimating()
                }
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as! ImageCollectionViewCell
        cell.activityIndicator.startAnimating()
        let url = content.getObjectAt(indexPath: indexPath).previewURL
        content.loadImage(loadURLString: url){ image, loadedURL, error in
            if let error = error {
                print(error)
                return
            }
            if loadedURL == url {
                DispatchQueue.main.async {
                    cell.imageView.image = image
                    cell.activityIndicator.stopAnimating()
                }
            }
        }
        return cell
    }
}
extension ImageCollectionViewController : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let paddingSpace = sectionInsets.left * (itemsPerRow + 1)
        let availableWidth = collectionView.bounds.width - paddingSpace
        let widthPerItem = availableWidth / itemsPerRow
        
        return CGSize(width: widthPerItem, height: widthPerItem)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
}
