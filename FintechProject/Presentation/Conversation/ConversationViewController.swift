//
//  ConversationViewController.swift
//  FintechProject
//
//  Created by Eugene Alekseev on 10/5/18.
//  Copyright © 2018 AlekseevEG. All rights reserved.
//

import UIKit
import MultipeerConnectivity
import CoreData

class ConversationViewController: UIViewController {
    
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet var keyboardHeightLayoutConstraint: NSLayoutConstraint?
    @IBOutlet weak var messageInputField: UITextField!
    
    let incomingTableViewCellID = "incomingMessageViewCellId"
    let outgoingTableViewCellID = "outgoingMessageViewCellId"
    
    var content : IConversationContent = DIContainer.shared.resolve()
    var isUserActive : Bool = false
    var titleLabel : UILabel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        content.viewDidLoad(delegate: self as NSFetchedResultsControllerDelegate)
        messageInputField.delegate = self
        configureTable()
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.keyboardNotification(notification:)),
                                               name: NSNotification.Name.UIKeyboardWillChangeFrame,
                                               object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.userChanged(_:)), name: NSNotification.Name(rawValue: "userChanged"), object: nil)
        messageInputField.addTarget(self, action: #selector(self.textFieldDidChange), for: .editingChanged)
        
        titleLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 200, height: 21))
        titleLabel?.center = CGPoint(x: 160, y: 285)
        titleLabel?.textAlignment = .center
        titleLabel?.text = content.chatPeer.name
        navigationItem.titleView = titleLabel
        updateTitle(isUserActive : isUserActive)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func textFieldDidChange() {
        if messageInputField.text?.isEmpty == true || !isUserActive{
            if self.sendButton.isEnabled  {
                updateButtonState(isActive: false)
            }
        }
        else {
            if !self.sendButton.isEnabled  {
                updateButtonState(isActive: true)
            }
        }
    }
    
    @objc func userChanged(_ notification: NSNotification) {
        if let user = notification.userInfo?["user"] as? User {
            if user.name == content.chatPeer.identifier{
                updateTitle(isUserActive: user.online)
                isUserActive = user.online
            }
        }
    }
    
    func updateTitle(isUserActive : Bool){
        let finalColor: UIColor = isUserActive ? UIColor.green : UIColor.black
        let finalFont : UIFont = isUserActive ? UIFont.systemFont(ofSize: 18) : UIFont.systemFont(ofSize: 17)
        let changeColor = CATransition()
        changeColor.type = kCATransitionFade
        changeColor.duration = 1.0
        
        CATransaction.begin()
        titleLabel?.textColor = finalColor
        titleLabel?.layer.add(changeColor, forKey: nil)
        titleLabel?.font = finalFont
        CATransaction.commit()
    }
    
    func updateButtonState(isActive: Bool){
        self.sendButton.isEnabled = isActive
        UIView.animate(withDuration: 0.25, animations: {
            self.sendButton.transform = CGAffineTransform(scaleX: 1.15, y: 1.15)
        },
                       completion: { _ in
                        UIView.animate(withDuration: 0.25) {
                            self.sendButton.transform = CGAffineTransform.identity
                        }
        })
    }
    
    func setChatPeer(peer: Peer){
        content.chatPeer = peer
    }
    private func configureTable() {
        tableView.dataSource = self
        tableView.delegate = self
        
        tableView.register(UINib(nibName: "IncomingMessageTableViewCell", bundle: nil), forCellReuseIdentifier: incomingTableViewCellID)
        tableView.register(UINib(nibName: "OutgoingMessageTableViewCell", bundle: nil), forCellReuseIdentifier: outgoingTableViewCellID)
        tableView.transform = CGAffineTransform(scaleX: 1, y: -1)
    }
    
    @objc func keyboardNotification(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            let endFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
            let endFrameY = endFrame?.origin.y ?? 0
            let duration:TimeInterval = (userInfo[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
            let animationCurveRawNSN = userInfo[UIKeyboardAnimationCurveUserInfoKey] as? NSNumber
            let animationCurveRaw = animationCurveRawNSN?.uintValue ?? UIViewAnimationOptions.curveEaseInOut.rawValue
            let animationCurve:UIViewAnimationOptions = UIViewAnimationOptions(rawValue: animationCurveRaw)
            if endFrameY >= UIScreen.main.bounds.size.height {
                self.keyboardHeightLayoutConstraint?.constant = 20.0
            } else {
                self.keyboardHeightLayoutConstraint?.constant = ((endFrame?.size.height)! + 20.0)
            }
            UIView.animate(withDuration: duration,
                           delay: TimeInterval(0),
                           options: animationCurve,
                           animations: { self.view.layoutIfNeeded() },
                           completion: nil)
        }
    }
    
    
    @IBAction func SendMessage() {
        self.view.endEditing(true)
        if sendButton.isEnabled {
            content.sendMessage(messageInputField.text)
            messageInputField.text = ""
        }
    }
}

extension ConversationViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.0001
    }
}

extension ConversationViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return content.getNumberOfObjects(section: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let message = content.getObjectAt(indexPath: indexPath)
        let cell = tableView.dequeueReusableCell(withIdentifier: message.incoming ? incomingTableViewCellID : outgoingTableViewCellID, for: indexPath) as! MessageTableViewCell
        cell.message = message.text
        cell.contentView.transform = CGAffineTransform(scaleX: 1, y: -1)
        return cell
    }
}

extension ConversationViewController : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        SendMessage()
        return false
    }
}

extension ConversationViewController : NSFetchedResultsControllerDelegate {
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.beginUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        switch type {
        case .insert:
            tableView.insertSections(IndexSet(integer: sectionIndex), with: .fade)
        case .delete:
            tableView.deleteSections(IndexSet(integer: sectionIndex), with: .fade)
        case .move:
            break
        case .update:
            break
        }
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch type {
        case .insert:
            tableView.insertRows(at: [newIndexPath!], with: .fade)
        case .delete:
            tableView.deleteRows(at: [indexPath!], with: .fade)
        case .update:
            tableView.reloadRows(at: [indexPath!], with: .fade)
        case .move:
            tableView.moveRow(at: indexPath!, to: newIndexPath!)
        }
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.endUpdates()
    }
}
