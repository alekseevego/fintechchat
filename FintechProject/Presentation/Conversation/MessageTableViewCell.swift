//
//  MessageTableViewCell.swift
//  FintechProject
//
//  Created by Pavel Alekseev on 10/7/18.
//  Copyright © 2018 AlekseevEG. All rights reserved.
//

import UIKit


class MessageTableViewCell: UITableViewCell, MessageCellConfiguration {

    @IBOutlet weak var messageLabel: UILabel!

    var message: String? {
        didSet {
            messageLabel.text = message
        }
    }
}
