//
//  ConversationContent.swift
//  FintechProject
//
//  Created by Pavel Alekseev on 11/18/18.
//  Copyright © 2018 AlekseevEG. All rights reserved.
//

import Foundation
import CoreData


protocol IConversationContent {
    
    var chatPeer : Peer! {get set}
    
    func viewDidLoad(delegate: NSFetchedResultsControllerDelegate)
    
    func saveMessage (id: String, text: String, conversationID: String)
    
    func getNumberOfObjects(section: Int) -> Int
    
    func getObjectAt(indexPath: IndexPath) -> StoredMessage
    
    func sendMessage(_ text: String?)
}

class ConversationContent : IConversationContent {
    
    var chatPeer : Peer!
    
    private let storage : IStorage
    
    private var communicationService : ICommunicationService?
    
    var fetchedResultsController : NSFetchedResultsController<StoredMessage>?
    
    init(storage: IStorage, communicationService: ICommunicationService) {
        self.storage = storage
        self.communicationService = communicationService
    }
    
    func viewDidLoad(delegate: NSFetchedResultsControllerDelegate)
    {
        fetchedResultsController = storage.initializeFetchedResultsController(delegate: delegate, entityName: String(describing: StoredMessage.self), sortDescriptors:  [NSSortDescriptor(key: "date", ascending: false)], predicate: nil) as? NSFetchedResultsController<StoredMessage>
    }
    
    func saveMessage (id: String, text: String, conversationID: String) {
        storage.saveMessage(id: id, text: text, incoming: false, conversationID: conversationID, date: Date())
    }
    
    func getNumberOfObjects(section: Int) -> Int {
        guard let sections = fetchedResultsController?.sections else {
            return 0
        }
        return sections[section].numberOfObjects
    }
    
    func getObjectAt(indexPath: IndexPath) -> StoredMessage {
        guard let message = fetchedResultsController?.object(at: indexPath) else {
            fatalError("Attempt to configure cell without a managed object")
        }
        return message
    }
    
    func sendMessage(_ text: String?){
        guard let text = text, !text.isEmpty
            else {
                return
        }
        let id = generateMessageID()
        saveMessage(id: id, text: text, conversationID: chatPeer.identifier)
        
        let message = Message(eventType: .textMessage, identifier: id, text: text, incoming: false)
        communicationService?.send(message, to: chatPeer)
    }
    private func generateMessageID() -> String {
        return ("\(arc4random_uniform(UINT32_MAX)) + \(Date.timeIntervalSinceReferenceDate)+\(arc4random_uniform(UINT32_MAX))".data(using: .utf8)?.base64EncodedString())!
    }
}
