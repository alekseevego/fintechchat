//
//  ConversationListViewController.swift
//  FintechProject
//
//  Created by Eugene Alekseev on 10/5/18.
//  Copyright © 2018 AlekseevEG. All rights reserved.
//

import UIKit
import MultipeerConnectivity
import CoreData

class ConversationListViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    let tableViewCellID = "conversationViewCellId"
    
    var content : IConversationListContent = DIContainer.shared.resolve()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        content.viewDidLoad(delegate: self as NSFetchedResultsControllerDelegate)//only reason to import coredata
        configureTable()
        let profileImage = UIImage(named: "ProfileImage")?.withRenderingMode(.alwaysOriginal)
        let barButton = UIBarButtonItem(image: profileImage, style: .plain, target: self, action: #selector(onProfileClick))
        navigationItem.setRightBarButton(barButton, animated: false)
    }
    
    private func configureTable() {
        tableView.register(UINib(nibName: "ConversationViewCell", bundle: nil), forCellReuseIdentifier: tableViewCellID)
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    @IBAction func onProfileClick() {
        let profileStoryBoard = UIStoryboard(name: "Profile", bundle: nil)
        guard let profileView = profileStoryBoard.instantiateViewController(withIdentifier: "ProfileViewController") as? ProfileViewController else {
            fatalError("cannot instantiate profile view")
        }
        present(profileView, animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        content.loadProfile(callback: { (image) in
            var safeImage = image
            if image == nil {
                safeImage = #imageLiteral(resourceName: "ProfileImage")
            }
            let barButton = UIBarButtonItem(image: safeImage!.withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(self.onProfileClick))
            DispatchQueue.main.async {
                self.navigationItem.setRightBarButton(barButton, animated: false)
            }
        })
    }
}

extension ConversationListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.0001
    }
}

extension ConversationListViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return content.getNumberOfObjects(section: section)
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if (section == 0) {
            return "Online"
        } else {
            return "History"
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        let conversation = content.getObjectAt(indexPath: indexPath)
        let profileStoryBoard = UIStoryboard(name: "Conversation", bundle: nil)
        let conversationView = profileStoryBoard.instantiateViewController(withIdentifier: "ConversationViewController") as! ConversationViewController
        conversationView.navigationItem.title = conversation.name
        if let user = conversation.users?.lastObject as? User {
             conversationView.isUserActive = user.online
        }
        conversationView.setChatPeer(peer: Peer(identifier: conversation.conversationID!, name: conversation.name!))
        navigationController?.pushViewController(conversationView, animated: true)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: tableViewCellID, for: indexPath) as! ConversationListTableViewCell
        let conversation = content.getObjectAt(indexPath: indexPath)
        cell.name = conversation.name
        if let mes = conversation.messages?.lastObject as? StoredMessage {
            cell.message = mes
            cell.date = mes.date as Date?
        }
        else{
            cell.message = nil
            cell.date = nil
        }
        if let user = conversation.users?.lastObject as? User {
            cell.online = user.online
        }
        return cell
    }
}
extension ConversationListViewController : NSFetchedResultsControllerDelegate {
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.beginUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        switch type {
        case .insert:
            tableView.insertSections(IndexSet(integer: sectionIndex), with: .fade)
        case .delete:
            tableView.deleteSections(IndexSet(integer: sectionIndex), with: .fade)
        case .move:
            break
        case .update:
            break
        }
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch type {
        case .insert:
            tableView.insertRows(at: [newIndexPath!], with: .fade)
        case .delete:
            tableView.deleteRows(at: [indexPath!], with: .fade)
        case .update:
            tableView.reloadRows(at: [indexPath!], with: .fade)
        case .move:
            tableView.moveRow(at: indexPath!, to: newIndexPath!)
        }
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.endUpdates()
    }
}
