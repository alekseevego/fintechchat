//
//  ConversationListContent.swift
//  FintechProject
//
//  Created by Pavel Alekseev on 11/18/18.
//  Copyright © 2018 AlekseevEG. All rights reserved.
//

import Foundation
import CoreData
import UIKit

protocol IConversationListContent {
    
    func viewDidLoad(delegate: NSFetchedResultsControllerDelegate)
    
    func loadProfile(callback: @escaping (UIImage?) -> ())
    
    func getNumberOfObjects(section: Int) -> Int
    
    func getObjectAt(indexPath: IndexPath) -> Conversation
}

class ConversationListContent : IConversationListContent {
    private let storage : IStorage
    
    var fetchedResultsController : NSFetchedResultsController<Conversation>?
    
    init(storage: IStorage, communicationService: ICommunicationService) {
        self.storage = storage
    }
    
    func viewDidLoad(delegate: NSFetchedResultsControllerDelegate)
    {
        fetchedResultsController = storage.initializeFetchedResultsController(delegate: delegate, entityName: String(describing: Conversation.self), sortDescriptors:  [NSSortDescriptor(key: "name", ascending: true)], predicate: nil) as? NSFetchedResultsController<Conversation>
    }
    
    func loadProfile(callback: @escaping (UIImage?) -> ()){
        storage.loadProfile(callback: { (image, _, _) in
            callback(image)
        })
    }
    
    func getNumberOfObjects(section: Int) -> Int {
        guard let sections = fetchedResultsController?.sections else {
            return 0
        }
        return sections[section].numberOfObjects
    }
    
    func getObjectAt(indexPath: IndexPath) -> Conversation {
        guard let conversation = fetchedResultsController?.object(at: indexPath) else {
            fatalError("Attempt to configure cell without a managed object")
        }
        return conversation
    }
}
