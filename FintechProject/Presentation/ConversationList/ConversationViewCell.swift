//
//  TableViewCell.swift
//  FintechProject
//
//  Created by Eugene Alekseev on 10/5/18.
//  Copyright © 2018 AlekseevEG. All rights reserved.
//

import UIKit

class ConversationListTableViewCell: UITableViewCell, ConversationCellConfiguration {

    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!

    private let dateFormatter = DateFormatter()

    var name: String? {
        didSet {
            nameLabel.text = name ?? "Unknown"
        }
    }

    var message: StoredMessage? {
        didSet {
            if message == nil {
                messageLabel.font = UIFont.systemFont(ofSize: 17, weight: .light)
                messageLabel.text = "No messages yet"
            } else {
                messageLabel.font = UIFont.systemFont(ofSize: 17)
                messageLabel.text = message?.text
            }
        }
    }

    var date: Date? {
        didSet {
            if let date = date {
                if Calendar.current.isDateInToday(date) {
                    dateFormatter.dateFormat = "HH:mm"
                } else {
                    dateFormatter.dateFormat = "dd MMM"
                }
                dateLabel.text = dateFormatter.string(from: date)
            } else {
                dateLabel.text = ""
            }
        }
    }

    var online: Bool = false {
        didSet {
            if online {
                contentView.backgroundColor = UIColor(red: 1.00, green: 1.00, blue: 0.93, alpha: 1.0)
            } else {
                contentView.backgroundColor = UIColor.white
            }
        }
    }

    var hasUnreadMessages: Bool = false {
        didSet {
            if (message == nil) {
                return
            }
            if hasUnreadMessages {
                messageLabel.font = UIFont.systemFont(ofSize: 17, weight: .bold)
            } else {
                messageLabel.font = UIFont.systemFont(ofSize: 17.0)
            }
        }
    }
}
