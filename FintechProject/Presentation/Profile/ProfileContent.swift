//
//  ProfileContent.swift
//  FintechProject
//
//  Created by Eugene Alekseev on 11/18/18.
//  Copyright © 2018 AlekseevEG. All rights reserved.
//

import Foundation
import UIKit

protocol IProfileContent {
    
    func loadProfile(callback: @escaping (UIImage?, String?, String?) -> ())
    
    func saveProfile(image: UIImage?, name: String?, description: String?)
    
}

class ProfileContent : IProfileContent {
    
    private let storage : IStorage
    
    init(storage: IStorage) {
        self.storage = storage
    }
    
    func saveProfile(image: UIImage?, name: String?, description: String?) {
        storage.saveProfile(image: image, name: name, description: description, callback: {})
    }
    
    func loadProfile(callback: @escaping (UIImage?, String?, String?) -> ()) {
        storage.loadProfile(callback: callback)
    }
}
