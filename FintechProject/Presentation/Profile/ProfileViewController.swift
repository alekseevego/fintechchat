//
//  ViewController.swift
//  FintechProject
//
//  Created by Eugene Alekseev on 9/21/18.
//  Copyright © 2018 AlekseevEG. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    @IBOutlet weak var topProfileConstraint: NSLayoutConstraint!
    @IBOutlet weak var profileConstraint: NSLayoutConstraint!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var editBtn: UIButton!
    @IBOutlet weak var saveBtnGCD: UIButton!
    @IBOutlet weak var saveBtnOp: UIButton!
    @IBOutlet weak var profileImageBtn: UIButton!
    @IBOutlet weak var descriptionText: UITextView!
    @IBOutlet weak var titleText: UITextField!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var content : IProfileContent = DIContainer.shared.resolve()
    
    var initialPositionDescriptionText: CGPoint?
    var initialPositionTitleText: CGPoint?
    
    var imageChanged: Bool = false
    var titleChanged: Bool = false
    var descriptionChanged: Bool = false
    
    let profileImageName = "profileImage.png"
    let titleTextName = "titleText"
    let descriptionTextName = "descriptionText"
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        profileConstraint.priority = .defaultHigh
        topProfileConstraint.priority = .defaultLow
        
        titleText.addTarget(self, action: #selector(onTitleChanged), for: .editingChanged)
        titleText.isEnabled = false
        descriptionText.isEditable = false
        descriptionText.delegate = self
        
        profileImage.layer.cornerRadius = 30.0
        profileImage.clipsToBounds = true
        
        saveBtnGCD.isHidden = true
        saveBtnOp.isHidden = true
        
        activityIndicator.isHidden = true
        
        editBtn.layer.cornerRadius = 20.0
        editBtn.layer.borderColor = UIColor.black.cgColor
        editBtn.layer.borderWidth = 2.0
        
        profileImageBtn.imageView?.contentMode = .scaleAspectFit
        profileImageBtn.imageEdgeInsets = UIEdgeInsetsMake(10.0, 10.0, 10.0, 10.0)
        profileImageBtn.layer.cornerRadius = 30.0
        
        loadData()
    }
    
    @objc func onTitleChanged(_ textField: UITextField) {
        titleChanged = true
    }
    
    @objc func onDescriptionChanged(_ textField: UITextField) {
        descriptionChanged = true
    }
    
    func loadData() {
        titleText.text = "Имя профиля"
        descriptionText.text = "Расскажите про себя..."
        titleText.font = UIFont.systemFont(ofSize: 20, weight: .ultraLight)
        descriptionText.font = UIFont.systemFont(ofSize: 14, weight: .ultraLight)
        content.loadProfile(callback: {[weak self] image, name, description in
            DispatchQueue.main.async {
                if name != nil && !(name?.isEmpty)! {
                    self?.titleText.text = name
                    self?.titleText.font = UIFont.boldSystemFont(ofSize: 20)
                }
                if description != nil && !(description?.isEmpty)! {
                    self?.descriptionText.text = description
                    self?.descriptionText.font = UIFont.systemFont(ofSize: 14)
                }
                if image != nil {
                    self?.profileImage.image = image
                }
            }
        })
    }
    
    func showAlert(title: String) -> (() -> ()) {
        return { [weak self] in
            let alert = UIAlertController(title: title, message: nil, preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            let repeatAction = UIAlertAction(title: "Повторить?", style: .default, handler: { [weak self](al: UIAlertAction!) in self?.loadData() })
            alert.addAction(ok)
            alert.addAction(repeatAction)
            DispatchQueue.main.async {
                self?.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String: Any]) {
        picker.dismiss(animated: true)
        
        guard let image = info[UIImagePickerControllerEditedImage] as? UIImage else {
            print("No image found")
            return
        }
        setImage(image: image)
    }
    func setImage(image: UIImage){
        DispatchQueue.main.async { [weak self] in
            self?.profileImage.image = image
            self?.content.saveProfile(image: image, name: nil, description: nil)
        }
        imageChanged = true
    }
    @IBAction func closeClicked(_ sendrer: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func imageClicked(_ sender: Any) {
        let actionSheet = UIAlertController(title: "Select image", message: "Select where to get image for your profile.", preferredStyle: .actionSheet)
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            actionSheet.addAction(UIAlertAction(title: "Take photo with camera", style: .default, handler: { action in
                let vc = UIImagePickerController()
                vc.sourceType = .camera
                vc.allowsEditing = true
                vc.delegate = self
                self.present(vc, animated: true)
            }))
        }
        actionSheet.addAction(UIAlertAction(title: "Select from the gallery", style: .default, handler: { action in
            let vc = UIImagePickerController()
            vc.sourceType = .photoLibrary
            vc.allowsEditing = true
            vc.delegate = self
            self.present(vc, animated: true)
        }))
        actionSheet.addAction(UIAlertAction(title: "Load from web", style: .default, handler: { action in
            let collectionStoryboard = UIStoryboard(name: "ImageCollectionView", bundle: nil)
            guard let collectionViewController = collectionStoryboard.instantiateViewController(withIdentifier: "ImageCollectionView") as? ImageCollectionViewController else {
                fatalError("cannot instantiate collection view")
            }
            self.present(collectionViewController, animated: true, completion: nil)
        }))
        self.present(actionSheet, animated: true)
    }
    
    @IBAction func onEditClick() {
        
        topProfileConstraint.priority = .defaultHigh
        profileConstraint.priority = .defaultLow
        saveBtnGCD.isHidden = false
        saveBtnOp.isHidden = false
        editBtn.isHidden = true
        profileImage.alpha = 0
        profileImageBtn.alpha = 0
        descriptionText.isEditable = true
        titleText.isEnabled = true
        titleText.becomeFirstResponder()
        initialPositionDescriptionText = descriptionText.center
        initialPositionTitleText = titleText.center
        descriptionText.backgroundColor = UIColor(red: 0.93, green: 0.96, blue: 0.981, alpha: 1)
        
        let duration:TimeInterval = TimeInterval(0.5)
        let animationCurveRaw = UIViewAnimationOptions.curveEaseInOut.rawValue
        let animationCurve:UIViewAnimationOptions = UIViewAnimationOptions(rawValue: animationCurveRaw)
        UIView.animate(withDuration: duration,
                       delay: TimeInterval(0),
                       options: animationCurve,
                       animations: { self.view.layoutIfNeeded() },
                       completion: nil)
    }
    
    func resetLayout() {
        profileConstraint.priority = .defaultHigh
        topProfileConstraint.priority = .defaultLow
        saveBtnGCD.isHidden = true
        saveBtnOp.isHidden = true
        editBtn.isHidden = false
        descriptionText.isEditable = false
        titleText.isEnabled = false
        descriptionText.backgroundColor = UIColor.white
        UIView.animate(withDuration: 0.5, animations: { [weak self] in
            self?.profileImage.alpha = 1
            self?.profileImageBtn.alpha = 1
            self?.descriptionText.center = (self?.initialPositionDescriptionText)!
            self?.titleText.center = (self?.initialPositionTitleText)!
        })
        saveBtnGCD.isEnabled = true
        saveBtnOp.isEnabled = true
    }
    
    @IBAction func onGCDSaveClick() {
        saveData()
    }
    
    @IBAction func onOPSaveClick() {
        saveData()
    }
    
    func saveData() {
        saveBtnGCD.isEnabled = false
        saveBtnOp.isEnabled = false
        activityIndicator.isHidden = false
        activityIndicator.startAnimating()
        DispatchQueue.main.async { [weak self] in
            var name: String? = nil
            var description: String? = nil
            if (self?.titleChanged)! {
                //errorHandle: (self?.showAlert(title: "Ошибка сохранения"))!)
                name = (self?.titleText.text)!
                self?.titleChanged = false
            }
            if (self?.descriptionChanged)! {
                description = (self?.descriptionText.text)!//, errorHandle: (self?.showAlert(title: "Ошибка сохранения"))!)
                self?.descriptionChanged = false
            }
            self?.content.saveProfile(image: nil, name: name, description: description)
            DispatchQueue.main.async {
                self?.activityIndicator.stopAnimating()
                self?.activityIndicator.isHidden = true
                self?.resetLayout()
            }
            
        }
    }
}

extension ProfileViewController: UITextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
        descriptionChanged = true
    }
}

