//
//  CommunicationService.swift
//  FintechProject
//
//  Created by Eugene Alekseev on 10/26/18.
//  Copyright © 2018 AlekseevEG. All rights reserved.
//

import Foundation
import MultipeerConnectivity

class CommunicationService: NSObject, ICommunicationService {
    
    var delegate: CommunicationServiceDelegate?
    let serviceType = "tinkoff-chat"
    let discoveryInfo = ["username": "alekseeveg"]
    var users: [String : MCPeerID] = [:]
    var online: Bool = false
    
    var serviceAdvertiser : MCNearbyServiceAdvertiser?
    var browser : MCNearbyServiceBrowser?
    var session : MCSession?
    let MyPeerID = MCPeerID(displayName: UIDevice.current.name)
    var storage: IStorage
    
    
    init(storage: IStorage){
        self.storage = storage
        serviceAdvertiser = MCNearbyServiceAdvertiser(peer: MyPeerID, discoveryInfo: discoveryInfo, serviceType: serviceType)
        browser = MCNearbyServiceBrowser(peer: MyPeerID, serviceType: serviceType)
        session = MCSession(peer: MyPeerID)
        super.init()
        serviceAdvertiser?.delegate = self
        browser?.delegate = self
        session?.delegate = self
        delegate = self
        browser?.startBrowsingForPeers()
        serviceAdvertiser?.startAdvertisingPeer()
    }
    
    deinit {
        browser?.stopBrowsingForPeers()
    }
    
    func send(_ message: Message, to peer: Peer) {
        do {
            if let jsonObject = try? JSONEncoder().encode(message){
                if let user = users[peer.identifier]{
                    try session?.send(jsonObject, toPeers: [user], with: .reliable)
                }
            }
        }
        catch {
            print("Cannot send message \(error)")
        }
    }
    func stop(){
        serviceAdvertiser?.stopAdvertisingPeer()
        browser?.stopBrowsingForPeers()
    }
}

extension CommunicationService : MCNearbyServiceAdvertiserDelegate {
    
    func advertiser(_ advertiser: MCNearbyServiceAdvertiser, didNotStartAdvertisingPeer error: Error) {
        print("didNotStartAdvertisingPeer: \(error)")
    }
    
    func advertiser(_ advertiser: MCNearbyServiceAdvertiser, didReceiveInvitationFromPeer peerID: MCPeerID, withContext context: Data?, invitationHandler: @escaping (Bool, MCSession?) -> Void) {
        print( "didReceiveInvitationFromPeer \(peerID)")
        if users["\(peerID)"] != nil {
            return
        }
        invitationHandler(true, session)
    }
}
extension CommunicationService : MCSessionDelegate {
    func session(_ session: MCSession, didReceiveCertificate certificate: [Any]?, fromPeer peerID: MCPeerID, certificateHandler: @escaping (Bool) -> Void) {
        certificateHandler(true)
    }
    
    func session(_ session: MCSession, peer peerID: MCPeerID, didChange state: MCSessionState) {
        print("peer \(peerID) didChangeState: \(state.rawValue)")
    }
    
    func session(_ session: MCSession, didReceive data: Data, fromPeer peerID: MCPeerID) {
        print("didReceiveData: \(data)")
        do {
            var message = try JSONDecoder().decode(Message.self, from: data)
            message.incoming = true
            delegate?.communicationService(self, didReceiveMessage: message, from: Peer(identifier: "\(peerID)", name: peerID.displayName) )
        }
        catch {
            print("Deserialization error")
        }
    }
    
    func session(_ session: MCSession, didReceive stream: InputStream, withName streamName: String, fromPeer peerID: MCPeerID) {
        
    }
    
    func session(_ session: MCSession, didStartReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, with progress: Progress) {
        print("Recieving 📪📪📪")
    }
    
    func session(_ session: MCSession, didFinishReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, at localURL: URL?, withError error: Error?) {
        print("error")
    }
    
}
extension CommunicationService : MCNearbyServiceBrowserDelegate {
    func browser(_ browser: MCNearbyServiceBrowser, didNotStartBrowsingForPeers error: Error) {
        print("❌didNotStartBrowsingForPeers: \(error)❌")
    }
    
    func browser(_ browser: MCNearbyServiceBrowser, foundPeer peerID: MCPeerID, withDiscoveryInfo info: [String : String]?) {
        print("♋️foundPeer: \(peerID)♋️")
        if users[peerID.displayName] != nil {
            return
        }
        browser.invitePeer(peerID , to: session!, withContext: nil, timeout: 1)
        users[peerID.displayName] = peerID
        print(users.count)
        delegate?.communicationService(self, didFoundPeer: Peer(identifier: peerID.displayName, name: peerID.displayName))
        
    }
    
    func browser(_ browser: MCNearbyServiceBrowser, lostPeer peerID: MCPeerID) {
        print("😔lostPeer: \(peerID)😔")
        if let index = users.index(where: {$0.value == peerID}) {
            delegate?.communicationService(self, didLostPeer: Peer(identifier: users.keys[index], name : peerID.displayName))
        }
    }
}
extension CommunicationService : CommunicationServiceDelegate {
    func communicationService(_ communicationService: ICommunicationService, didFoundPeer peer: Peer) {
        storage.saveUser(id: peer.name, name: peer.name, online: true)
        storage.saveConversation(id: peer.name, name: peer.name, messageID: nil, userID: peer.name)
    }
    
    func communicationService(_ communicationService: ICommunicationService, didLostPeer peer: Peer) {
        storage.saveUser(id: peer.name, name: peer.name, online: false)
        storage.saveConversation(id: peer.name, name: peer.name, messageID: nil, userID: peer.name)
    }
    
    func communicationService(_ communicationService: ICommunicationService, didNotStartBrowsingForPeers error: Error) {
    }
    
    func communicationService(_ communicationService: ICommunicationService, didReceiveInviteFromPeer peer: Peer, invintationClosure: (Bool) -> Void) {
        
    }
    
    func communicationService(_ communicationService: ICommunicationService, didNotStartAdvertisingForPeers error: Error) {
        
    }
    
    func communicationService(_ communicationService: ICommunicationService, didReceiveMessage message: Message, from peer: Peer) {
        storage.saveMessage(id: message.identifier, text: message.text, incoming : message.incoming, conversationID: peer.name, date: Date())
        storage.saveUser(id: peer.name, name: peer.name, online: true)
        storage.saveConversation(id: peer.name, name: peer.name, messageID: message.identifier, userID: peer.name)
    }
}
