//
//  ImageLoaderService.swift
//  FintechProject
//
//  Created by Pavel Alekseev on 11/25/18.
//  Copyright © 2018 AlekseevEG. All rights reserved.
//

import Foundation
import UIKit

protocol IImageLoaderService {
    func loadImageList(completion: @escaping ([Hit], NSError?) ->())
    func loadImage(loadURLString: String, _ completion: @escaping (UIImage?, String, NSError?) -> ())
}

class ImageLoaderService : IImageLoaderService {
    
    let apikey = "10791173-baf9f7d8fa23186ad477704de"
    let searchString = "yellow+flowers"
    var webClient : IWebClient
    
    init(webClient: IWebClient){
        self.webClient = webClient
    }
    
    func loadImageList(completion: @escaping (_ results: [Hit], _ error: NSError?) ->()){
        webClient.loadData(urlString: "https://pixabay.com/api/?key=\(apikey)&q=\(searchString)&image_type=photo&pretty=true&per_page=100") { data, error in
            guard let data = data else {
                completion([], nil)
                print("Data loaded was nil")
                return
            }
            do{
                let result = try JSONDecoder().decode(PixabayResult.self, from: data as Data)
                completion(result.hits, nil)
            }
            catch {
                print("Cannot send message \(error)")
                completion([], error as NSError)
            }
        }
    }
    
    func loadImage(loadURLString: String, _ completion: @escaping (UIImage?, String, NSError?) -> ()) {
        webClient.loadData(urlString: loadURLString) { data, error in
            if let error = error {
                completion(nil, loadURLString, error as NSError?)
                return
            }
            
            guard let data = data else {
                completion(nil, loadURLString, nil)
                return
            }
            let returnedImage = UIImage(data: data)
            completion(returnedImage, loadURLString, nil)
        }
    }
}
