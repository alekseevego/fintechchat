//
//  PixabayResult.swift
//  FintechProject
//
//  Created by Pavel Alekseev on 11/24/18.
//  Copyright © 2018 AlekseevEG. All rights reserved.
//

import Foundation
struct PixabayResult : Codable {
    var hits : [Hit]
}
struct Hit : Codable {
    var largeImageURL : String
    var previewURL : String
    var id : Int
}
