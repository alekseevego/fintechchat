//
//  LoadedPic.swift
//  FintechProject
//
//  Created by Pavel Alekseev on 11/24/18.
//  Copyright © 2018 AlekseevEG. All rights reserved.
//

import Foundation
import UIKit

class LoadedPic {
    var thumbnail : UIImage?
    var largeImage : UIImage?
}
